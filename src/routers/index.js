import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const  routes = [
  { path: '/', component: require ('./../components/Home.vue') }
]
export default new VueRouter ({
  routes
})