import Vue from 'vue'
import App from './App.vue'
import router from './routers'
import store from './store'
import './styles/main.scss'
import vuetify from '@/plugins/vuetify'
import vSelect from 'vue-select'
Vue.config.productionTip = false

Vue.component(vSelect)

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app')
