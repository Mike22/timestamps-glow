import * as types from './mutation-types'

export default {
  [types.POST_ARTICLE] (state, { articlePayload }) {
    state.articlePayload = articlePayload
  }
}